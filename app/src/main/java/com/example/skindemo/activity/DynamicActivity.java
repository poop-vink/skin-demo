package com.example.skindemo.activity;

import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;


import com.example.lib_skin.SkinManager;
import com.example.skindemo.R;

/**
 * @ClassName: DynamicActivity
 * @Author: 史大拿
 * @CreateDate: 1/5/23$ 9:26 AM$
 * TODO 自定义换肤
 */
public class DynamicActivity extends SkinBaseActivity {

    private TextView mTv;

    @Override
    protected void initCreate(Bundle savedInstanceState) {

        mTv = findViewById(R.id.tv);
        mTv.setBackground(SkinManager.getInstance().getDrawable("global_skin_drawable_background"));
        mTv.setText(SkinManager.getInstance().getString("global_custom_view_text"));
    }

    @Override
    protected void notifyChanged() {
        Toast.makeText(this, "notifyChanged", Toast.LENGTH_SHORT).show();
        mTv.setBackground(SkinManager.getInstance().getDrawable("global_skin_drawable_background"));
        mTv.setText(SkinManager.getInstance().getString("global_custom_view_text"));
    }

    @Override
    protected int layoutId() {
        return R.layout.activity_dynamic;
    }
}
