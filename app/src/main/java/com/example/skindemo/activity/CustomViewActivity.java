package com.example.skindemo.activity;

import android.os.Bundle;

import com.example.skindemo.R;

/**
 * @ClassName: CustomViewActivity
 * @Author: 史大拿
 * @CreateDate: 1/4/23$ 4:57 PM$
 * TODO
 */
public class CustomViewActivity extends SkinBaseActivity {


    @Override
    protected void initCreate(Bundle savedInstanceState) {

    }

    @Override
    protected int layoutId() {
        return R.layout.activity_custom_view;
    }
}
