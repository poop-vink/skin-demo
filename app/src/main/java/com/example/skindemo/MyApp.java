package com.example.skindemo;

import android.app.Application;
import android.util.Log;

import com.example.lib_skin.SkinManager;

/**
 * @ClassName: MyApp
 * @Author: 史大拿
 * @CreateDate: 12/28/22$ 1:29 PM$
 * TODO
 */
public class MyApp extends Application {
    @Override
    public void onCreate() {
        super.onCreate();

        SkinManager.init(this);

    }
}
