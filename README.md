
-- app 

-- skin-pack-making [用来生成皮肤包]

-- lib-skin [换肤框架代码]


第一篇: [Android 换肤之资源(Resources)加载源码分析(一)](https://juejin.cn/post/7182471289524158523)

    下载源码: git clone -b level-simple https://gitee.com/lanyangyangzzz/skin-demo.git 
    
第二篇: [android setContentView() LayoutInflater源码解析](https://juejin.cn/post/7135710282227777573)

第三篇: [android 换肤框架搭建及使用 (3 完结篇) ](https://juejin.cn/post/7185433671355072573)

     下载源码: git clone https://gitee.com/lanyangyangzzz/skin-demo.git 


# 优化:
2023-1-11:
出现问题:   多套皮肤无法替换
出现原因:
```
# SkinManager.java
public void loadSkin(String path, boolean isNotify, @Nullable Activity activity) {
        if (skinResource == null) {
            SkinResource.init(mApplication, path);
            skinResource = SkinResource.getInstance();
        }
}
```
资源加载过后不会执行init方法重新加载新皮肤
优化后:    已解决

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

2023-1-7:
出现问题:   刷新的时候会刷新所有保存的view
优化后:    只刷新当前activity保存的view,避免过度刷新